<?php

namespace App\Validator;

class SmallLetter
{
    public function apply($field, $value, $attribute)
    {
        if (!preg_match('/^[a-záàâãéèêíïóôõöúçñ]+$/', $value)) {
            return false;
        }

        return true;
    }
}