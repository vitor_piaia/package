<?php

namespace App\Validator;

/**
 * Class CEP
 * @package App\Validator
 */
class CEP
{
    /**
     * @param $field
     * @param $value
     * @param $attribute
     * @return bool
     */
    public function apply($field, $value, $attribute)
    {
        return \App\Utilities\CEP::valid($value, true);
    }
}
