<?php

namespace App\Validator;

class Money
{
    public function apply($field, $value, $attribute)
    {
        return ..\Utilities\Money::valid($value);
    }
}
