<?php

namespace App\Validator;

class Percentage
{
    public function apply($field, $value, $attribute)
    {
        return \App\Utilities\Percentage::valid($value);
    }
}