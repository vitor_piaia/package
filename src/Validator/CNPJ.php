<?php

namespace App\Validator;

class CNPJ
{
    public function apply($field, $value, $attribute)
    {
        if(empty($value)) {
            return true;
        }

        return \App\Utilities\CNPJ::valid($value);
    }
}